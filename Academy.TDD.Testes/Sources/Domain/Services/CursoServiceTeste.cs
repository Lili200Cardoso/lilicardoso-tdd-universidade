﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Domain.Services
{
    [Trait("Service", "Service Curso")]
    public class CursoServiceTeste
    {
        private readonly Mock<ICursoRepository> _mockCursoRepository;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        private readonly Fixture _fixture;
        private readonly Claim[] _claims;

        public CursoServiceTeste()
        {
            _mockCursoRepository = new Mock<ICursoRepository>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _fixture = FixtureConfig.Get();
            _claims = _fixture.Create<Usuario>().Claims();

        }

        [Fact(DisplayName = "Lista cursos")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Curso>>();

            _mockCursoRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<Curso, bool>>>())).ReturnsAsync(entities);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new CursoService(_mockCursoRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterTodosAsync();

            Assert.True(response.ToList().Count() > 0);
        }

        [Fact(DisplayName = "Busca curso Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Curso>();

            _mockCursoRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<Curso, bool>>>())).ReturnsAsync(entity);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new CursoService(_mockCursoRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Curso")]
        public async Task Post()
        {
            var entity = _fixture.Create<Curso>();

            _mockCursoRepository.Setup(mock => mock.AddAsync(It.IsAny<Curso>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new CursoService(_mockCursoRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AdicionarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Edita Curso Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Curso>();

            _mockCursoRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<Curso, bool>>>())).ReturnsAsync(entity);
            _mockCursoRepository.Setup(mock => mock.EditAsync(It.IsAny<Curso>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new CursoService(_mockCursoRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AlterarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Remove Curso Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Curso>();

            _mockCursoRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockCursoRepository.Setup(mock => mock.RemoveAsync(It.IsAny<Curso>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new CursoService(_mockCursoRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.DeletarAsync(entity.Id);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
   
}
