﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Services;
using Academy.DDD.Domain.Settings;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Bogus;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Domain.Services
{
    [Trait("Service", "Service Endereco")]
    public class EnderecoServiceTeste
    {
        private readonly Mock<IEnderecoRepository> _mockEnderecoRepository;
        private readonly Mock<ILocalidadeRepository> _mockLocalidadeAccessor;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        //private readonly Mock<AppSetting> _mockAppSetting;
        private readonly Faker _faker;
        private readonly Fixture _fixture;

        public EnderecoServiceTeste()
        {
            _mockEnderecoRepository = new Mock<IEnderecoRepository>();
            _faker = new Faker();
            _fixture = FixtureConfig.Get();
            _mockLocalidadeAccessor = new Mock<ILocalidadeRepository>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            //_mockAppSetting = new Mock<AppSetting>();
        }

        [Fact(DisplayName = "Busca Localidade cep")]
        public async Task GetLocalidade()
        {
            var entity = _fixture.Create<Localidade>();

            _mockLocalidadeAccessor.Setup(mock => mock.GetLocalidadeAsync(It.IsAny<string>())).ReturnsAsync(entity);

            var service = new EnderecoService(_mockEnderecoRepository.Object, _mockLocalidadeAccessor.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterLocalidadeAsync(entity.Cep.ToString());

            Assert.Equal(response.Cep, entity.Cep);
        }

        [Fact(DisplayName = "Lista Enderecos")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Endereco>>();

            _mockEnderecoRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<Endereco, bool>>>())).ReturnsAsync(entities);


            var service = new EnderecoService(_mockEnderecoRepository.Object, _mockLocalidadeAccessor.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterTodosAsync();

            Assert.True(response.ToList().Count() > 0);
        }

        [Fact(DisplayName = "Busca Endereco por Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Endereco>();

            _mockEnderecoRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<Endereco, bool>>>())).ReturnsAsync(entity);

            var service = new EnderecoService(_mockEnderecoRepository.Object, _mockLocalidadeAccessor.Object,  _mockHttpContextAccessor.Object);

            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Endereco")]
        public async Task Post()
        {
            var entity = _fixture.Create<Endereco>();

            _mockEnderecoRepository.Setup(mock => mock.AddAsync(It.IsAny<Endereco>())).Returns(Task.CompletedTask);

            var service = new EnderecoService(_mockEnderecoRepository.Object, _mockLocalidadeAccessor.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AdicionarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Edita Endereco existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Endereco>();

            _mockEnderecoRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<Endereco, bool>>>())).ReturnsAsync(entity);
            _mockEnderecoRepository.Setup(mock => mock.EditAsync(It.IsAny<Endereco>())).Returns(Task.CompletedTask);

            var service = new EnderecoService(_mockEnderecoRepository.Object, _mockLocalidadeAccessor.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AlterarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Remove Endereco existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Endereco>();

            _mockEnderecoRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockEnderecoRepository.Setup(mock => mock.RemoveAsync(It.IsAny<Endereco>())).Returns(Task.CompletedTask);

            var service = new EnderecoService(_mockEnderecoRepository.Object, _mockLocalidadeAccessor.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.DeletarAsync(entity.Id);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
