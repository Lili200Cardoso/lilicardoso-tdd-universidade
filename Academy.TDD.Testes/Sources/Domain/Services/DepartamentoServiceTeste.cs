﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Domain.Services
{
    [Trait("Service", "Service Departamento")]
    public class DepartamentoServiceTeste
    {
        private readonly Mock<IDepartamentoRepository> _mockDepartamentoRepository;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        private readonly Fixture _fixture;
        private readonly Claim[] _claims;

        public DepartamentoServiceTeste()
        {
            _mockDepartamentoRepository = new Mock<IDepartamentoRepository>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _fixture = FixtureConfig.Get();
            _claims = _fixture.Create<Usuario>().Claims();
        }

        [Fact(DisplayName = "Lista Departamentos")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Departamento>>();

            _mockDepartamentoRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<Departamento, bool>>>())).ReturnsAsync(entities);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new DepartamentoService(_mockDepartamentoRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterTodosAsync();

            Assert.True(response.ToList().Count() > 0);
        }

        [Fact(DisplayName = "Busca Departamento Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Departamento>();

            _mockDepartamentoRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<Departamento, bool>>>())).ReturnsAsync(entity);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new DepartamentoService(_mockDepartamentoRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra Departamento")]
        public async Task Post()
        {
            var entity = _fixture.Create<Departamento>();

            _mockDepartamentoRepository.Setup(mock => mock.AddAsync(It.IsAny<Departamento>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new DepartamentoService(_mockDepartamentoRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AdicionarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Edita Departamento Existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Departamento>();

            _mockDepartamentoRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<Departamento, bool>>>())).ReturnsAsync(entity);
            _mockDepartamentoRepository.Setup(mock => mock.EditAsync(It.IsAny<Departamento>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new DepartamentoService(_mockDepartamentoRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.AlterarAsync(entity);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        [Fact(DisplayName = "Remove Departamento Existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Departamento>();

            _mockDepartamentoRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockDepartamentoRepository.Setup(mock => mock.RemoveAsync(It.IsAny<Departamento>())).Returns(Task.CompletedTask);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(_claims);

            var service = new DepartamentoService(_mockDepartamentoRepository.Object, _mockHttpContextAccessor.Object);

            try
            {
                await service.DeletarAsync(entity.Id);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }
    }
}
