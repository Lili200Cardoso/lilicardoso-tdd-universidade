﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Bogus;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Domain.Services
{
    [Trait("Service", "Service de Professores")]
    public class ProfessorServiceTeste
    {
        private readonly Mock<IProfessorRepository> _mockProfessorRepository;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        private readonly Fixture _fixture;
        private readonly Faker _fake;

        public ProfessorServiceTeste()
        {
            _mockProfessorRepository = new Mock<IProfessorRepository>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _fixture = FixtureConfig.Get();
            _fake = new Faker();

        }

        [Theory(DisplayName = "Busca um professor por Id")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task GetById(string perfil)
        {
            var entity = _fixture.Create<Professor>();
            var claims = ClaimConfig.Get(_fake.UniqueIndex, _fake.Person.FullName, _fake.Person.Email, perfil);

            _mockProfessorRepository.Setup(mock => mock.FindAsync(It.IsAny<Expression<Func<Professor, bool>>>())).ReturnsAsync(entity);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);
            
            var response = await service.ObterPorIdAsync(entity.Id);

            Assert.Equal(response.Id, entity.Id);
        }


        [Theory(DisplayName = "Busca todos professores")]
        [InlineData("Aluno")]
        [InlineData("Professor")]
        public async Task Get(string perfil)
        {
            var entities = _fixture.Create<List<Professor>>();
            var claims = ClaimConfig.Get(_fake.UniqueIndex, _fake.Person.FullName, _fake.Person.Email, perfil);

            _mockProfessorRepository.Setup(mock => mock.ListAsync(It.IsAny<Expression<Func<Professor, bool>>>())).ReturnsAsync(entities);
            _mockHttpContextAccessor.Setup(mock => mock.HttpContext.User.Claims).Returns(claims);

            var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

            var response = await service.ObterTodosAsync();

            Assert.True(response.Count() > 0);
        }


        [Fact(DisplayName = "Cadastra um novo professor")]
        public async Task Post()
        {
            var entity = _fixture.Create<Professor>();

            _mockProfessorRepository.Setup(mock => mock.AddAsync(It.IsAny<Professor>())).Returns(Task.CompletedTask);

            var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

            var exception = await Record.ExceptionAsync(() =>  service.AdicionarAsync(entity));
            Assert.Null(exception);

        }


        [Fact(DisplayName = "Atualiza um professor existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Professor>();

            _mockProfessorRepository.Setup(mock => mock.FindAsNoTrackingAsync(It.IsAny<Expression<Func<Professor, bool>>>())).ReturnsAsync(entity);
            _mockProfessorRepository.Setup(mock => mock.EditAsync(It.IsAny<Professor>())).Returns(Task.CompletedTask);

            var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);

            var exception = await Record.ExceptionAsync(() => service.AlterarAsync(entity));
            Assert.Null(exception);
        }


        [Fact(DisplayName = "Remove um professor existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Professor>();

            _mockProfessorRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).ReturnsAsync(entity);
            _mockProfessorRepository.Setup(mock => mock.RemoveAsync(It.IsAny<Professor>())).Returns(Task.CompletedTask);

            var service = new ProfessorService(_mockProfessorRepository.Object, _mockHttpContextAccessor.Object);


            var exception = await Record.ExceptionAsync(() => service.DeletarAsync(entity.Id));
            Assert.Null(exception);
        }
    }
}
