﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Infrastructure.Contexts;
using Academy.DDD.Infrastructure.Repositories;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Infrastructure.Repositories
{
    [Trait("Repository", "Repository Professor")]
    public class ProfessorRepositoryTeste
    {
        private readonly Fixture _fixture;
        private readonly Mock<AcademyContext> _mockAcademyContext;

        public ProfessorRepositoryTeste()
        {
            _fixture = FixtureConfig.Get();
            _mockAcademyContext = new Mock<AcademyContext>(new DbContextOptionsBuilder<AcademyContext>().UseLazyLoadingProxies().Options);
        }

        [Fact(DisplayName = "Listar todos professores")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Professor>>();

            _mockAcademyContext.Setup(mock => mock.Set<Professor>()).ReturnsDbSet(entities);

            var repository = new ProfessorRepository(_mockAcademyContext.Object);

            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Listar professor por id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Professor>();

            _mockAcademyContext.Setup(mock => mock.Set<Professor>().FindAsync(It.IsAny<int>())).ReturnsAsync( entity);

            var repository = new ProfessorRepository(_mockAcademyContext.Object);

            var id = entity.Id;
            var response = await repository.FindAsync(id);

            Assert.Equal(response.Id, id);
        }

        [Fact(DisplayName = "Cadastrar um novo professor")]
        public async Task Post()
        {
            var entity = _fixture.Create<Professor>();

            _mockAcademyContext.Setup(mock => mock.Set<Professor>()).ReturnsDbSet(new List<Professor>());

            var repository = new ProfessorRepository(_mockAcademyContext.Object);

            var exception = await Record.ExceptionAsync(() => repository.AddAsync(entity));

            Assert.Null(exception);
        }

        [Fact(DisplayName = "Alterar um professor existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Professor>();

            _mockAcademyContext.Setup(mock => mock.Set<Professor>()).ReturnsDbSet(new List<Professor>());

            var repository = new ProfessorRepository(_mockAcademyContext.Object);

            var exception = await Record.ExceptionAsync(() => repository.EditAsync(entity));

            Assert.Null(exception);
        }

        [Fact(DisplayName = "Excluir um professor existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Professor>();

            _mockAcademyContext.Setup(mock => mock.Set<Professor>()).ReturnsDbSet(new List<Professor>());

            var repository = new ProfessorRepository(_mockAcademyContext.Object);

            var exception = await Record.ExceptionAsync(() => repository.RemoveAsync(entity));

            Assert.Null(exception);
        }

    }
}
