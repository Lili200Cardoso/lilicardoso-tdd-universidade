﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Exceptions;
using Academy.DDD.Infrastructure.Repositories;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Bogus;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Infrastructure.Repositories
{
    public class LocalidadeRepositoryTeste
    {
        private readonly Fixture _fixture;
        private readonly Faker _faker;
        private readonly Mock<HttpMessageHandler> _mockHttpMessageHandler;

        public LocalidadeRepositoryTeste()
        {
            _fixture = FixtureConfig.Get();
            _faker = new Faker();
            _mockHttpMessageHandler = new Mock<HttpMessageHandler>();   
        }

        [Fact(DisplayName = "Busacar Localidade pelo Cep")]
        public async Task GetByCep()
        {
            var entity = _fixture.Create<Localidade>();

            var httpResposnemessage = new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = JsonContent.Create(entity)
            };

            _mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                                ItExpr.IsAny<HttpRequestMessage>(),
                                ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(httpResposnemessage);

            var url = _faker.Internet.Url();
            var httpCliente = new HttpClient(_mockHttpMessageHandler.Object)
            {
                BaseAddress = new Uri(url)
            };

            var repository = new LocalidadeRepository(httpCliente);

            var response = await repository.GetLocalidadeAsync(entity.Cep.ToString());

            Assert.Equal(response.Cep, entity.Cep);
        }

        [Fact(DisplayName = "Buscar Localidade pelo Cep com erro 500")]
        public async Task GetByCepErro500()
        {
            var cep = _faker.Address.ZipCode();

            var httpResposnemessage = new HttpResponseMessage()
            {
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Content = JsonContent.Create("Erro ao fazer a consulta.")
            };

            _mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                                ItExpr.IsAny<HttpRequestMessage>(),
                                ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(httpResposnemessage);

            var url = _faker.Internet.Url();

            var httpCliente = new HttpClient(_mockHttpMessageHandler.Object)
            {
                BaseAddress = new Uri(url)
            };

            var repository = new LocalidadeRepository(httpCliente);

            await Assert.ThrowsAnyAsync<InformacaoException>(() => repository.GetLocalidadeAsync(cep));
        }
    }
}
