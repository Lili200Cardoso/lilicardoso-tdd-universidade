﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Infrastructure.Contexts;
using Academy.DDD.Infrastructure.Repositories;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.Infrastructure.Repositories
{
    [Trait("Repository", "Repository Perfil")]
    public class PerfilRepositoryTeste
    {
        private readonly Fixture _fixture;
        private readonly Mock<AcademyContext> _mockAcademyContext;

        public PerfilRepositoryTeste()
        {
            _fixture = FixtureConfig.Get();
            _mockAcademyContext = new Mock<AcademyContext>(new DbContextOptionsBuilder<AcademyContext>().UseLazyLoadingProxies().Options);
        }

        [Fact(DisplayName = "Listar todos perfis")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Perfil>>();

            _mockAcademyContext.Setup(mock => mock.Set<Perfil>()).ReturnsDbSet(entities);

            var repository = new PerfilRepository(_mockAcademyContext.Object);

            var response = await repository.ListAsync();

            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Listar perfil por id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Perfil>();

            _mockAcademyContext.Setup(mock => mock.Set<Perfil>().FindAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var repository = new PerfilRepository(_mockAcademyContext.Object);

            var id = entity.Id;
            var response = await repository.FindAsync(id);

            Assert.Equal(response.Id, id);
        }

        [Fact(DisplayName = "Cadastrar um novo perfil")]
        public async Task Post()
        {
            var entity = _fixture.Create<Perfil>();

            _mockAcademyContext.Setup(mock => mock.Set<Perfil>()).ReturnsDbSet(new List<Perfil>());

            var repository = new PerfilRepository(_mockAcademyContext.Object);

            var exception = await Record.ExceptionAsync(() => repository.AddAsync(entity));

            Assert.Null(exception);
        }

        [Fact(DisplayName = "Alterar um perfil existente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Perfil>();

            _mockAcademyContext.Setup(mock => mock.Set<Perfil>()).ReturnsDbSet(new List<Perfil>());

            var repository = new PerfilRepository(_mockAcademyContext.Object);

            var exception = await Record.ExceptionAsync(() => repository.EditAsync(entity));

            Assert.Null(exception);
        }

        [Fact(DisplayName = "Excluir um perfil existente")]
        public async Task Delete()
        {
            var entity = _fixture.Create<Perfil>();

            _mockAcademyContext.Setup(mock => mock.Set<Perfil>()).ReturnsDbSet(new List<Perfil>());

            var repository = new PerfilRepository(_mockAcademyContext.Object);

            var exception = await Record.ExceptionAsync(() => repository.RemoveAsync(entity));

            Assert.Null(exception);
        }
    }
}
