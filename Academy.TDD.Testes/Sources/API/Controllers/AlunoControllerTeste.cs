﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{
    [Trait("Controller", "Controller de Alunos")]
    public class AlunoControllerTeste
    {
        private readonly Mock<IAlunoService> _mockAlunoService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public AlunoControllerTeste()
        {
            _mockAlunoService = new Mock<IAlunoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }


        [Fact(DisplayName = "Busca um aluno por Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Aluno>();

            _mockAlunoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new AlunosController(_mapper, _mockAlunoService.Object);

            var response = await controller.GetByIdAsync(entity.Id);

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var professorResponse = Assert.IsType<AlunoResponse>(objectResult.Value);
            Assert.Equal(professorResponse.Id, entity.Id);
        }

        [Fact(DisplayName = "Busca todos alunos")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Aluno>>();

            _mockAlunoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new AlunosController(_mapper, _mockAlunoService.Object);

            var response = await controller.GetAsync();

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var alunoResponse = Assert.IsType<List<AlunoResponse>>(objectResult.Value);
            Assert.True(alunoResponse.Count() > 0);
        }

        [Fact(DisplayName = "Cadastra um novo aluno")]
        public async Task Post()
        {
            var request = _fixture.Create<AlunoRequest>();

            _mockAlunoService.Setup(mock => mock.AdicionarAsync(It.IsAny<Aluno>())).Returns(Task.CompletedTask);

            var controller = new AlunosController(_mapper, _mockAlunoService.Object);

            var response = await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(response);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Atualiza um aluno existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<AlunoRequest>();

            _mockAlunoService.Setup(mock => mock.AlterarAsync(It.IsAny<Aluno>())).Returns(Task.CompletedTask);

            var controller = new AlunosController(_mapper, _mockAlunoService.Object);

            var response = await controller.PutAsync(id, request);

            var objectResult = Assert.IsType<NoContentResult>(response);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Remove um aluno existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockAlunoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new AlunosController(_mapper, _mockAlunoService.Object);

            var response = await controller.DeleteAsync(id);

            var objectResult = Assert.IsType<NoContentResult>(response);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Busca aluno por matricula")]
        public async Task Patch()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<AlunoMatriculaRequest>();

            _mockAlunoService.Setup(mock => mock.AtualizarMatriculaAsync(It.IsAny<int>(), It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new AlunosController(_mapper, _mockAlunoService.Object);

            var actionResult = await controller.PatchAsync(id, request);

            var objectResult = Assert.IsType<OkResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

    }
}
