﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{
    [Trait("Controller", "Controller Cursos")]
    public class CursosControllerTest
    {
        private readonly Mock<ICursoService> _mockCursoService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public CursosControllerTest()
        {
            _mockCursoService = new Mock<ICursoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Lista Cursos")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<Curso>>();

            _mockCursoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new CursosController(_mapper, _mockCursoService.Object);

            var actionResult = await controller.GetAsync();

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<CursoResponse>>(objectResult.Value);
            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Curso Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Curso>();

            _mockCursoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new CursosController(_mapper, _mockCursoService.Object);

            var actionResult = await controller.GetByIdAsync(entity.Id);

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<CursoResponse>(objectResult.Value);
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Busca Curso por Nome")]
        public async Task GetNome()
        {
            var entity = _fixture.Create<List<Curso>>();

            _mockCursoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entity);

            var controller = new CursosController(_mapper, _mockCursoService.Object);

            var actionResult = await controller.GetAsync(entity.ToString());

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<CursoResponse>>(objectResult.Value);
            Assert.NotNull(response);
        }


        [Fact(DisplayName = "Cadastra Curso")]
        public async Task Post()
        {
            var request = _fixture.Create<CursoRequest>();

            _mockCursoService.Setup(mock => mock.AdicionarAsync(It.IsAny<Curso>())).Returns(Task.CompletedTask);

            var controller = new CursosController(_mapper, _mockCursoService.Object);

            var actionResult = await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Edita Curso Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<CursoRequest>();

            _mockCursoService.Setup(mock => mock.AlterarAsync(It.IsAny<Curso>())).Returns(Task.CompletedTask);

            var controller = new CursosController(_mapper, _mockCursoService.Object);

            var actionResult = await controller.PutAsync(id, request);

            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Remove Curso Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockCursoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new CursosController(_mapper, _mockCursoService.Object);

            var actionResult = await controller.DeleteAsync(id);

            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);


        }


    }
}