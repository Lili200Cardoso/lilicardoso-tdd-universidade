﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{

    [Trait("Controller", "Controller Departamentos")]
    public class DepartamentosControllerTest
    {
        private readonly Mock<IDepartamentoService> _mockDepartamentoService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public DepartamentosControllerTest()
        {
            _mockDepartamentoService = new Mock<IDepartamentoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Lista departamentos")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<Departamento>>();

            _mockDepartamentoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new DepartamentosController(_mapper, _mockDepartamentoService.Object);

            var actionResult = await controller.GetAsync();

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<DepartamentoResponse>>(objectResult.Value);
            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Departamento Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Departamento>();

            _mockDepartamentoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new DepartamentosController(_mapper, _mockDepartamentoService.Object);

            var actionResult = await controller.GetByIdAsync(entity.Id);

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<DepartamentoResponse>(objectResult.Value);
            Assert.Equal(response.Id, entity.Id);
        }

        [Fact(DisplayName = "Busca Departamento por Nome")]
        public async Task GetNome()
        {
            var entity = _fixture.Create<List<Departamento>>();

            _mockDepartamentoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entity);

            var controller = new DepartamentosController(_mapper, _mockDepartamentoService.Object);

            var actionResult = await controller.GetAsync(entity.ToString());

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<DepartamentoResponse>>(objectResult.Value);
            Assert.NotNull(response);
        }

        [Fact(DisplayName = "Cadastra Departamento")]
        public async Task Post()
        {
            var request = _fixture.Create<DepartamentoRequest>();

            _mockDepartamentoService.Setup(mock => mock.AdicionarAsync(It.IsAny<Departamento>())).Returns(Task.CompletedTask);

            var controller = new DepartamentosController(_mapper, _mockDepartamentoService.Object);

            var actionResult = await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Edita Departamento Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<DepartamentoRequest>();

            _mockDepartamentoService.Setup(mock => mock.AlterarAsync(It.IsAny<Departamento>())).Returns(Task.CompletedTask);

            var controller = new DepartamentosController(_mapper, _mockDepartamentoService.Object);

            var actionResult = await controller.PutAsync(id, request);

            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Remove Departamento Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<DepartamentoRequest>();

            _mockDepartamentoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new DepartamentosController(_mapper, _mockDepartamentoService.Object);

            var actionResult = await controller.DeleteAsync(id);

            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }
    }
}