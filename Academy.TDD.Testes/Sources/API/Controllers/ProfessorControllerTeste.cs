﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{
    [Trait("Controller", "Controller de Professores")]
    public class ProfessorControllerTeste
    {
        private readonly Mock<IProfessorService> _mockProfessorService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public ProfessorControllerTeste()
        {
            _mockProfessorService = new Mock<IProfessorService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();

        }

        [Fact(DisplayName = "Busca um professor por Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Professor>();

            _mockProfessorService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new ProfessoresController(_mapper, _mockProfessorService.Object);

            var response = await controller.GetByIdAsync(entity.Id);

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var professorResponse = Assert.IsType<ProfessorResponse>(objectResult.Value);
            Assert.Equal(professorResponse.Id, entity.Id);
        }


        [Fact(DisplayName = "Busca todos professores")]
        public async Task Get()
        {
            var entities = _fixture.Create<List<Professor>>();

            _mockProfessorService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new ProfessoresController(_mapper, _mockProfessorService.Object);

            var response = await controller.GetAsync();

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var professoresResponse = Assert.IsType<List<ProfessorResponse>>(objectResult.Value);
            Assert.True(professoresResponse.Count() > 0);
        }


        [Fact(DisplayName = "Cadastra um novo professor")]
        public async Task Post()
        {
            var request = _fixture.Create<ProfessorRequest>();

            _mockProfessorService.Setup(mock => mock.AdicionarAsync(It.IsAny<Professor>())).Returns(Task.CompletedTask);

            var controller = new ProfessoresController(_mapper, _mockProfessorService.Object);

            var response = await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(response);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Atualiza um professor existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<ProfessorRequest>();

            _mockProfessorService.Setup(mock => mock.AlterarAsync(It.IsAny<Professor>())).Returns(Task.CompletedTask);

            var controller = new ProfessoresController(_mapper, _mockProfessorService.Object);

            var response = await controller.PutAsync(id, request);

            var objectResult = Assert.IsType<NoContentResult>(response);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Remove um professor existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockProfessorService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new ProfessoresController(_mapper, _mockProfessorService.Object);

            var response = await controller.DeleteAsync(id);

            var objectResult = Assert.IsType<NoContentResult>(response);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }
    }
}
