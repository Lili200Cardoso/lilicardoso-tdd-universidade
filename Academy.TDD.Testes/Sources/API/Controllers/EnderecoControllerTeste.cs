﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using Bogus;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{
    [Trait("Controller", "Controller Enderecos")]
    public class EnderecosControllerTeste
    {
        private readonly Mock<IEnderecoService> _mockEnderecoService;
        private readonly Mock<IHttpContextAccessor> _mockHttpContextAccessor;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;
        private readonly Faker _fake;

        public EnderecosControllerTeste()
        {
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _mockEnderecoService = new Mock<IEnderecoService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
            _fake = new Faker();
        }

        [Fact(DisplayName = "Busca Localidade cep")]
        public async Task GetNacionalidade()
        {
            var entity = _fixture.Create<Localidade>();

            _mockEnderecoService.Setup(mock => mock.ObterLocalidadeAsync(It.IsAny<string>())).ReturnsAsync(entity);

            var controller = new EnderecosController(_mapper, _mockEnderecoService.Object);

            var actionResult = await controller.GetLocalidadeAsync(entity.Cep.ToString());

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<LocalidadeResponse>(objectResult.Value);
            Assert.NotNull(response.Cep);
        }

        [Fact(DisplayName = "Lista Enderecos")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<Endereco>>();

            _mockEnderecoService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entities);

            var controller = new EnderecosController(_mapper, _mockEnderecoService.Object);

            var actionResult = await controller.GetAsync();

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<EnderecoResponse>>(objectResult.Value);
            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Endereco por Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Endereco>();

            _mockEnderecoService.Setup(mock => mock.ObterPorIdAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new EnderecosController(_mapper, _mockEnderecoService.Object);

            var response = await controller.GetByIdAsync(entity.Id);

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var enderecoResponse = Assert.IsType<EnderecoResponse>(objectResult.Value);
            Assert.Equal(enderecoResponse.Id, entity.Id);
        }

        [Fact(DisplayName = "Cadastra um novo Endereco")]
        public async Task Post()
        {
            var request = new EnderecoRequest()
            {
                Cep = _fake.Address.ZipCode(),
                Rua = _fake.Address.StreetName(),
                Cidade = _fake.Address.City(),
                Estado = _fake.Address.Country(),
                Complemento = _fake.Address.StreetName()
            };

            _mockEnderecoService.Setup(mock => mock.AdicionarAsync(It.IsAny<Endereco>())).Returns(Task.CompletedTask);

            var controller = new EnderecosController(_mapper, _mockEnderecoService.Object);

            var response =  await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(response);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Atualiza um Endereco existente")]
        public async Task Put()
        {
            var id  = _fake.IndexFaker;
            var request = new EnderecoRequest()
            {
                Cep = _fake.Address.ZipCode(),
                Rua = _fake.Address.StreetName(),
                Cidade = _fake.Address.City(),
                Estado = _fake.Address.Country(),
                Complemento = _fake.Address.StreetName()
            };

            _mockEnderecoService.Setup(mock => mock.AlterarAsync(It.IsAny<Endereco>())).Returns(Task.CompletedTask);

            var controller = new EnderecosController(_mapper, _mockEnderecoService.Object);

            var response = await controller.PutAsync(id, request);

            var objectResult = Assert.IsType<NoContentResult>(response);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Remove um Endereco existente")]
        public async Task Delete()
        {
            var id = _fake.IndexFaker;

            _mockEnderecoService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new EnderecosController(_mapper, _mockEnderecoService.Object);

            var response = await controller.DeleteAsync(id);

            var objectResult = Assert.IsType<NoContentResult>(response);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

    }
}
