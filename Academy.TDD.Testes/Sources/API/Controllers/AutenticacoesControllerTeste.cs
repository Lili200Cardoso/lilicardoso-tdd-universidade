﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{
    [Trait("Controller", "Controller Autenticacoes")]
    public class AutenticacoesControllerTest
    {
        private readonly Mock<IUsuarioService> _mockUsuarioService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public AutenticacoesControllerTest()
        {
            _mockUsuarioService = new Mock<IUsuarioService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Cria Token")]
        public async Task Post()
        {
            var request = _fixture.Create<AutenticacaoRequest>();
            var response = _fixture.Create<AutenticacaoResponse>();

            _mockUsuarioService.Setup(mock => mock.AutenticarAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(response);

            var controller = new AutenticacoesController(_mockUsuarioService.Object);

            var actionResult = await controller.PostAsync(request);

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var responseResult = Assert.IsType<AutenticacaoResponse>(objectResult.Value);
            Assert.Equal(responseResult.Token, response.Token);
        }
    }

}
