﻿using Academy.DDD.API.Controllers;
using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using Academy.TDD.Testes.Configs;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Academy.TDD.Testes.Sources.API.Controllers
{
    [Trait("Controller", "Controller Usuarios")]
    public class UsuariosControllerTest
    {
        private readonly Mock<IUsuarioService> _mockUsuarioService;
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public UsuariosControllerTest()
        {
            _mockUsuarioService = new Mock<IUsuarioService>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Lista Usuarios")]
        public async Task GetAsync()
        {
            var entities = _fixture.Create<List<Usuario>>();

            _mockUsuarioService.Setup(mock => mock.ObterTodosUsuarioAsync()).ReturnsAsync(entities);

            var controller = new UsuariosController(_mapper, _mockUsuarioService.Object);

            var actionResult = await controller.GetAsync();

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<UsuarioResponse>>(objectResult.Value);
            Assert.True(response.Count() > 0);
        }

        [Fact(DisplayName = "Busca Usuario Id")]
        public async Task GetById()
        {
            var entity = _fixture.Create<Usuario>();

            _mockUsuarioService.Setup(mock => mock.ObterPorIdUsuarioAsync(It.IsAny<int>())).ReturnsAsync(entity);

            var controller = new UsuariosController(_mapper, _mockUsuarioService.Object);

            var actionResult = await controller.GetByIdAsync(entity.Id);

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<UsuarioResponse>(objectResult.Value);
            Assert.Equal(response.Id, entity.Id);
        }


        [Fact(DisplayName = "Busca Usuario por Nome")]
        public async Task GetNome()
        {
            var entity = _fixture.Create<List<Usuario>>();

            _mockUsuarioService.Setup(mock => mock.ObterTodosAsync()).ReturnsAsync(entity);

            var controller = new UsuariosController(_mapper, _mockUsuarioService.Object);

            var actionResult = await controller.GetAsync(entity.ToString());

            var objectResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            var response = Assert.IsType<List<UsuarioResponse>>(objectResult.Value);
            Assert.NotNull(response);
        }


        [Fact(DisplayName = "Cadastra Usuario")]
        public async Task Post()
        {
            var request = _fixture.Create<UsuarioRequest>();

            _mockUsuarioService.Setup(mock => mock.AdicionarAsync(It.IsAny<Usuario>())).Returns(Task.CompletedTask);

            var controller = new UsuariosController(_mapper, _mockUsuarioService.Object);

            var actionResult = await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(actionResult);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }


        [Fact(DisplayName = "Edita Usuario Existente")]
        public async Task Put()
        {
            var id = _fixture.Create<int>();
            var request = _fixture.Create<UsuarioRequest>();

            _mockUsuarioService.Setup(mock => mock.AlterarAsync(It.IsAny<Usuario>())).Returns(Task.CompletedTask);

            var controller = new UsuariosController(_mapper, _mockUsuarioService.Object);

            var actionResult = await controller.PutAsync(id, request);

            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Remove Usuario Existente")]
        public async Task Delete()
        {
            var id = _fixture.Create<int>();

            _mockUsuarioService.Setup(mock => mock.DeletarAsync(It.IsAny<int>())).Returns(Task.CompletedTask);

            var controller = new UsuariosController(_mapper, _mockUsuarioService.Object);

            var actionResult = await controller.DeleteAsync(id);

            var objectResult = Assert.IsType<NoContentResult>(actionResult);
            Assert.Equal(StatusCodes.Status204NoContent, objectResult.StatusCode);

        }
    }
}
