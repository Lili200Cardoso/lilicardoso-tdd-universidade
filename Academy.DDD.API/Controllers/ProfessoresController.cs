﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Academy.DDD.Domain.Utils.ConstanteUtil;

namespace Academy.DDD.API.Controllers
{
    [Authorize(Roles = PerfilProfessorNome)]
    public class ProfessoresController : BaseController<Professor, ProfessorRequest, ProfessorResponse>
    {
        private readonly IMapper _mapper;
        private readonly IProfessorService _professorService;
        public ProfessoresController(IMapper mapper, IProfessorService professorService) : base(mapper, professorService)
        {
            _mapper = mapper;
            _professorService = professorService;
        }


        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] ProfessorSalarioRequest request)
        {
            await _professorService.AtualizarSalarioAsync(id, request.Salario);
            return Ok();
        }

        [HttpGet("salario")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ProfessorResponse>>> GetAsync([FromQuery] float salario)
        {
            var entities = await _professorService.ObterTodosAsync(x => x.Salario.Equals(salario));
            var response = _mapper.Map<List<ProfessorResponse>>(entities);
            return Ok(response);
        }
    }
}
