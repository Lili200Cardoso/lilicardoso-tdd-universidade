﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Academy.DDD.API.Controllers
{
    public class EnderecosController : BaseController<Endereco, EnderecoRequest, EnderecoResponse>
    {
        private readonly IMapper _mapper;
        private readonly IEnderecoService _enderecoService;

        public EnderecosController(IMapper mapper, IEnderecoService service) : base(mapper, service)
        {
            _mapper = mapper;
            _enderecoService = service;
        }
       
         [HttpGet("localidade")]
         [ProducesResponseType(200)]
         public async Task<ActionResult<LocalidadeResponse>> GetLocalidadeAsync([FromQuery] string cep)
         {
            var entity = await _enderecoService.ObterLocalidadeAsync(cep);
            var response = _mapper.Map<LocalidadeResponse>(entity);
            return Ok(response);
                
         }

        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] EnderecoComplementoRequest request)
        {
            await _enderecoService.AtualizarComplementoAsync(id, request.Complemento);
            return Ok();
        }

        [HttpGet("complemento")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<EnderecoResponse>>> GetAsync([FromQuery] string complemento)
        {
            var entities = await _enderecoService.ObterTodosAsync(x => x.Complemento.Equals(complemento));
            var response = _mapper.Map<List<EnderecoResponse>>(entities);
            return Ok(response);
        }

    }
}
