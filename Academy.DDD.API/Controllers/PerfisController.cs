﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Academy.DDD.API.Controllers
{
    public class PerfisController : BaseController<Perfil, PerfilRequest, PerfilResponse>
    {
        private readonly IMapper _mapper;
        private readonly IPerfilService _perfilService;
        public PerfisController(IMapper mapper, IPerfilService perfilService) : base(mapper, perfilService)
        {
            _mapper = mapper;
            _perfilService = perfilService;

        }

        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] PerfilNomeRequest request)
        {
            await _perfilService.AtualizarNomeAsync(id, request.Nome);
            return Ok();
        }

        [HttpGet("nome")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<PerfilResponse>>> GetAsync([FromQuery] string nome)
        {
            var entities = await _perfilService.ObterTodosAsync(x => x.Nome.Contains(nome));
            var response = _mapper.Map<List<PerfilResponse>>(entities);
            return Ok(response);
        }
    }
}