﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Academy.DDD.Domain.Utils.ConstanteUtil;

namespace Academy.DDD.API.Controllers
{
    [Authorize(Roles = PerfilAlunoNome)]
    public class CursosController : BaseController<Curso, CursoRequest, CursoResponse>
    {
        private readonly IMapper _mapper;
        private readonly ICursoService _cursoService;

        public CursosController(IMapper mapper, ICursoService cursoService) : base(mapper, cursoService)
        {
            _mapper = mapper;
            _cursoService = cursoService;
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] CursoTipoCursoRequest request)
        {
            await _cursoService.AtualizarTipoCursoAsync(id, request.TipoCurso);
            return Ok();
        }

        [HttpGet("tipoCurso")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<CursoResponse>>> GetAsync([FromQuery] string tipoCurso)
        {
            var entities = await _cursoService.ObterTodosAsync(x => x.TipoCurso.Equals(tipoCurso));
            var response = _mapper.Map<List<CursoResponse>>(entities);
            return Ok(response);
        }
    }
}