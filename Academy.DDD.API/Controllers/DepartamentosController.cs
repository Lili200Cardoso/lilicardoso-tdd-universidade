﻿using Academy.DDD.Domain.Contracts.Request;
using Academy.DDD.Domain.Contracts.Response;
using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Academy.DDD.Domain.Utils.ConstanteUtil;

namespace Academy.DDD.API.Controllers
{
    [Authorize(Roles = PerfilProfessorNome)]
    public class DepartamentosController : BaseController<Departamento, DepartamentoRequest, DepartamentoResponse>
    {
        private readonly IMapper _mapper;
        private readonly IDepartamentoService _departamentoService;
        public DepartamentosController(IMapper mapper, IDepartamentoService departamentoService) : base(mapper, departamentoService)
        {
            _mapper = mapper;
            _departamentoService = departamentoService;
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> PatchAsync([FromRoute] int id, [FromBody] DepartamentoNomeRequest request)
        {
            await _departamentoService.AtualizarNomeAsync(id, request.Nome);
            return Ok();
        }

        [HttpGet("nome")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<DepartamentoResponse>>> GetAsync([FromQuery] string nome)
        {
            var entities = await _departamentoService.ObterTodosAsync(x => x.Nome.Contains(nome));
            var response = _mapper.Map<List<DepartamentoResponse>>(entities);
            return Ok(response);
        }
    }
}
