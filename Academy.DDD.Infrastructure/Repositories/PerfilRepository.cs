﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Infrastructure.Contexts;

namespace Academy.DDD.Infrastructure.Repositories
{
    public class PerfilRepository : BaseRepository<Perfil>, IPerfilRepository
    {
        public PerfilRepository(AcademyContext academyContext) : base(academyContext)
        {
        }
    }
}
