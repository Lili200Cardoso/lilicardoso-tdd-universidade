﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Infrastructure.Contexts;

namespace Academy.DDD.Infrastructure.Repositories
{
    public class EnderecoRepository : BaseRepository<Endereco>, IEnderecoRepository
    {
        public EnderecoRepository(AcademyContext academyContext) : base(academyContext)
        {
        }
    }
}
