﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Infrastructure.Contexts;

namespace Academy.DDD.Infrastructure.Repositories
{
    public class DepartamentoRepository : BaseRepository<Departamento>, IDepartamentoRepository
    {
        public DepartamentoRepository(AcademyContext academyContext) : base(academyContext)
        {
        }
    }
}
