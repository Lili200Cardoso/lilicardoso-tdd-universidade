# Nesse exercício prático dessa semana iremos abordar o conteúdo apresentado nas aulas sobre de TDD.
Considerem a modelagem utilizada na Semana 9, de DDD, para base da codificação.



Observações:
01. Os testes deveram ter cobertura de no mínimo de 70%;
02. Deverá ter teste na regra de aplicação, na regra de negócio e no repositório;
03. Para ter um exercício que atenda todos os critérios de avaliação, é indicado ter como base o projeto de exemplo, criado na aula da semana 10;
04. Em todos os testes deverá ser utilizado o Mock;
05. Todos os métodos deveram ser async/await.



