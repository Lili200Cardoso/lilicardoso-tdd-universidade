﻿namespace Academy.DDD.Domain.Settings
{
    public class AppSetting
    {
        public string SqlServerConnection { get; set; }
        public string ApiLocalidade { get; set; }
        public string JwtSecurityKey { get; set; }
    }
}
