﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Utils
{
    public static class ConstanteUtil
    {
        public const string PerfilProfessorNome = "Professor";
        public const string PerfilAlunoNome = "Aluno";
    }
}
