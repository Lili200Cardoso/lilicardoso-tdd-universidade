﻿using Academy.DDD.Domain.Entities;

namespace Academy.DDD.Domain.Interfaces.Repositories
{
    public interface ILocalidadeRepository
    {
        Task<Localidade> GetLocalidadeAsync(string cep);
    }
}
