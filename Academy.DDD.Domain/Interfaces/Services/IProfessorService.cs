﻿using Academy.DDD.Domain.Entities;

namespace Academy.DDD.Domain.Interfaces.Services
{
    public interface IProfessorService : IBaseService<Professor>
    {
        Task AtualizarSalarioAsync(int id, float salario );
    }
}
