﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Interfaces.Services
{
    public interface IDepartamentoService : IBaseService<Departamento>
    {
        Task AtualizarNomeAsync(int id, string nome);
    }
}
