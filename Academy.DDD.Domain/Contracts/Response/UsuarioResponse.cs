﻿using Academy.DDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Response
{
    public class UsuarioResponse : BaseResponse
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public  PerfilResponse Perfil { get; set; }
        public EnderecoResponse Endereco { get; set; }
    }
}
