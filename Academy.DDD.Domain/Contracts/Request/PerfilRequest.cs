﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Request
{
    public class PerfilRequest
    {
        public string Nome { get; set; }
    }
}
