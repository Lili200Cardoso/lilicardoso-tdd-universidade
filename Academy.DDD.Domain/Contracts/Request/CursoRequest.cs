﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Request
{
    public class CursoRequest
    {
        public int? DepartamentoId { get; set; }
        public string Turno { get; set; }
        public int Duracao { get; set; }
        public string TipoCurso { get; set; }
    }
}
