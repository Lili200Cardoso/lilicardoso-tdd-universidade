﻿using Academy.DDD.Domain.Entities;

namespace Academy.DDD.Domain.Contracts.Request
{
    public class AlunoRequest
    {
        public int? CursoId { get; set; }
        public int? UsuarioId { get; set; }
        public int Matricula { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
    }
}
