﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Request
{
    public class EnderecoRequest
    {
        [Required(ErrorMessage = "O campo 'Cep' é obrigatorio")]
        [RegularExpression(@"(^[0-9]{5})-?([0-9]{3}$)", ErrorMessage = "Cep Inválido")]
        public string Cep { get; set; }
        public string Rua { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Complemento { get; set; }
        public string Localidade { get; set; }
    }
}
