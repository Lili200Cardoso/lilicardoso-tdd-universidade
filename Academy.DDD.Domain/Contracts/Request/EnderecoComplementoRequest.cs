﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Contracts.Request
{
    public class EnderecoComplementoRequest
    {
        public string Complemento { get; set; }
    }
}
