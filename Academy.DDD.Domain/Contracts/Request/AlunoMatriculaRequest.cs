﻿namespace Academy.DDD.Domain.Contracts.Request
{
    public class AlunoMatriculaRequest
    {
        public int Matricula { get; set; }
    }
}
