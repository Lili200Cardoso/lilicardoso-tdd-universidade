﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Entities
{
    public class Aluno : BaseEntity
    {
        public int? UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }
        public int? CursoId { get; set; }
        public virtual Curso Curso { get; set; }
        public int Matricula { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

    }
}        
