﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Entities
{
    public class Departamento : BaseEntity
    {
        public int? EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
        public string Nome { get; set; }

    }
}
