﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Services
{
    public class PerfilService : BaseService<Perfil>, IPerfilService
    {
        private readonly IPerfilRepository _perfilRepository;
        public PerfilService(IPerfilRepository perfilRepository, IHttpContextAccessor httpContextAccessor) : base(perfilRepository, httpContextAccessor)
        {
            _perfilRepository = perfilRepository;

        }

        public async Task AtualizarNomeAsync(int id, string nome)
        {
            var entity = await ObterPorIdAsync(id);
            entity.Nome = nome;
            entity.DataAlteracao = DateTime.Now;
            entity.UsuarioAlteracao = UserId;
            await _perfilRepository.EditAsync(entity);
        }
    }
}
