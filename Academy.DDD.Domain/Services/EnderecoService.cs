﻿using Academy.DDD.Domain.Entities;
using Academy.DDD.Domain.Interfaces.Repositories;
using Academy.DDD.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.DDD.Domain.Services
{
    public class EnderecoService : BaseService<Endereco>, IEnderecoService
    {
        private readonly ILocalidadeRepository _localidadeRepository;
        private readonly IEnderecoRepository _enderecoRepository;
        public EnderecoService(IEnderecoRepository enderecoRepository, ILocalidadeRepository localidadeRepository, IHttpContextAccessor httpContextAccessor) : base(enderecoRepository, httpContextAccessor)
        {
            _localidadeRepository = localidadeRepository;
            _enderecoRepository = enderecoRepository;
        }

        public async Task<Localidade> ObterLocalidadeAsync(string cep)
        {
            return await  _localidadeRepository.GetLocalidadeAsync(cep);
        }

        public async Task AtualizarComplementoAsync(int id, string complemento)
        {
            var entity = await ObterPorIdAsync(id);
            entity.Complemento = complemento;
            entity.DataAlteracao = DateTime.Now;
            entity.UsuarioAlteracao = UserId;
            await _enderecoRepository.EditAsync(entity);
        }
    }
}
